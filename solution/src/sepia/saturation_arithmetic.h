#ifndef _SATURATION_ARITHMETIC
#define _SATURATION_ARITHMETIC

#include <inttypes.h>

uint8_t sat(uint64_t value);

#endif
